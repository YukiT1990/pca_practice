# Reference
# https://qiita.com/g-k/items/e5bdb7564d1bff16502a

from sklearn.datasets import load_boston
import pandas as pd
import numpy as np

boston = load_boston()

df = pd.DataFrame(boston.data, columns=boston.feature_names)
la = boston.feature_names


class PCA:

    def __init__(self):
        # Standardisation of data データの標準化
        self.standardscaler = None
        # Covariance matrix 分散共分散行列
        self.v_cov = None
        # Eigenvalue 固有値 
        self.eig = None
        # Eigenvectors 固有ベクトル
        self.eig_vec = None

    def fit(self, x):
        # Standardise your data データを標準化する
        self.standardscaler = (x - x.mean())/x.std(ddof=0)
        # Find the variance-covariance matrix of the data データの分散共分散行列を求める
        self.v_cov = np.cov(self.standardscaler.T, bias = 0)
        # Find the eigenvalues and eigenvectors of the variance-covariance matrix 分散共分散行列の固有値と固有ベクトルを求める
        self.eig, self.eig_vec = np.linalg.eig(self.v_cov)

        # Matrix of eigenvectors for each principal component and variable 各主成分と変数の固有ベクトルをマトリクス化
        self.pca_matrix = pd.DataFrame(self.eig_vec.T, columns = df.columns)
        self.pca_matrix.index = ['PC' + str(i+1) for i in self.pca_matrix.index]
        self.pca_matrix = self.pca_matrix.T
        self.pca_matrix.loc['固有値'] = list(self.eig)
        # Find the contribution rate 寄与率を求める
        self.pca_matrix.loc['寄与率'] = self.pca_matrix.loc['固有値'] / self.pca_matrix.loc['固有値'].sum()
        self.pca_matrix['']=(['固有ベクトル']*len(la) + ['',''])
        self.pca_matrix = self.pca_matrix.set_index('', append = True).swaplevel()

    def transfer(self, n_components):
        # Mapping data into PCA (principal component) space PCA(主成分)空間にデータを写像する
        return np.dot(self.standardscaler, self.eig_vec[:,:n_components])


pca = PCA()

pca.fit(df)

pca.pca_matrix

print(pca.transfer(n_components = 3))


# from sklearn import datasets
# california_housing = datasets.fetch_california_housing()
# california_housing_df = pd.DataFrame(california_housing.data, columns=california_housing.feature_names)
# la = california_housing.feature_names

# pca2 = PCA()

# pca2.fit(california_housing_df)
# ValueError: Shape of passed values is (8, 8), indices imply (8, 13)

# pca2.pca_matrix

# print(pca2.transfer(n_components = 3))